<br>
<div class="row">
  <div class="col-md-12 text-center">
    <h2>Editar Categoria de Examen Clinico</h2>
  </div>
</div>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/categorias/procesarActualizacion" method="post" id="frm_nuevo_hcat2">
    <input type="hidden" class="form-control" value="<?php echo $categoria->id_hcat; ?>" name="id_hcat" id="id_hcat" value="" placeholder="Por favor ingrese la identificación">
    <br>
    <br>
    <label for="">NOMBRE:</label>
    <input class="form-control"  type="text" name="nombre_hcat" id="nombre_hcat"  value="<?php echo $categoria->nombre_hcat; ?>"placeholder="Por favor Ingrese el nombre">
    <br>
    <br>

    <label for="">DESCRIPCIÓN:</label>
    <input class="form-control"  type="text" name="descripcion_hcat" id="descripcion_hcat" value="<?php echo $categoria->descripcion_hcat; ?>" placeholder="Por favor Ingrese la dirección">
    <br>
    <br>

    <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>

    <a href="<?php echo site_url(); ?>/categorias/index" class="btn btn-warning">CANCELAR</a>

</form>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_hcat2").validate({
      rules:{
        nombre_hcat:{
          letras:true,
          required:true
        },

        descripcion_hcat:{
          required:true
        },
      },
      messages:{
        nombre_hcat:{
          letras:"El nombre solo acepta letras",
          required:"Por favor ingrese el nombre"
        },

        descripcion_hcat:{
          required:"Por favor ingrese la descripción"
        },

      }
    });
</script>
