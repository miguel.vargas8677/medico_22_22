<br>
<div class="row">
  <div class="col-md-12 text-center">
    <h2>Editar Paciente</h2>
  </div>
</div>
<div class="row" style="margin: 0 20px 0 20px;">
  <div class="col-md-12">
    <div class="row">

      <!--Primera columna -->
      <br>
      <div class="col-md-6">

        <form action="<?php echo site_url(); ?>/clientes/procesarActualizacion" method="post" id="frm_nuevo_cliente">

          <br>
          <br>

          <input type="hidden" class="form-control" value="<?php echo $cliente->id_cli; ?>" name="id_cli" id="id_cli" value="" placeholder="Por favor ingrese la identificación">



          <label for="">Identificación: </label> <br>
          <input type="text" class="form-control" value="<?php echo $cliente->identificacion_cli; ?>" name="identificacion_cli" id="identificacion_cli" value="" placeholder="Por favor ingrese la identificación">
          <br>

          <label for="">Apellido:</label> <br>
          <input class="form-control" type="text" value="<?php echo $cliente->apellido_cli; ?>" name="apellido_cli" id="apellido_cli"  value="" placeholder="Ingrese el apellido">
          <br>

          <label for="">Nombre:</label> <br>
          <input class="form-control" type="text" value="<?php echo $cliente->nombre_cli; ?>" name="nombre_cli" id="nombre_cli"  value="" placeholder="Ingrese el nombre">
          <br>

          <label for="">Telefono:</label> <br>
          <input class="form-control" type="text" value="<?php echo $cliente->telefono_cli; ?>" name="telefono_cli" id="telefono_cli"  value="" placeholder="Ingrese su teléfono">
          <br>

          <label for="">Dirección:</label> <br>
          <input type="text" class="form-control" value="<?php echo $cliente->direccion_cli; ?>" name="direccion_cli" id="direccion_cli"  value="" placeholder="Ingrese su dirección">
          <br><br>

          <label for="">E-mail:</label> <br>
          <input class="form-control" type="email" value="<?php echo $cliente->email_cli; ?>" name="email_cli" id="email_cli"  value="" placeholder="Ingrese su email">
          <br>

          <label for="">ESTADO</label>
          <select class="form-control" name="estado_cli" id="estado_cli">
              <option value="">--Seleccione--</option>
              <option value="ACTIVO">ACTIVO</option>
              <option value="INACTIVO">INACTIVO</option>
          </select>
          </input>
          <br><br>
          <label for="">PAIS</label>
          <select   class="form-control" name="fk_id_pais" id="fk_id_pais" required>
            <option value="">---seleccione un pais---</option>
            <!-- validar -->
            <?php if ($listadoPaises):     ?>
              <?php foreach ($listadoPaises->result() as $paisTemporal): ?>
                <option value="<?php echo $paisTemporal->id_pais; ?>">
                <?php echo $paisTemporal->nombre_pais; ?>
                </option>
              <?php endforeach ?>
            <?php endif; ?>
          <br>

              </select>




          <!--
          <select class="form-control" name="estado_cli" id="estado_cli">
            <option value="">
              Seleccione una opción
            </option>
            <option value="Activo">
              Activo
            </option>
            <option value="Inactivo">
              Inactivo
            </option>

          </select>

          -->


          <br>

          <button class="btn btn-primary" name="button" type="submit"> <a style=" color:white;">Actualizar</a> </button>
          &nbsp&nbsp
          <button class="btn btn-primary"> <a href="<?php echo site_url(); ?>/clientes/index" style=" color:white;"><i class="fa fa-times"> Cancelar </i></a> </button>
          <br><br>
        </form>

      </div><br>


  </div>  <!--termina primer row interno -->





</div>  <!--cierre col-md-12 -->
</div> <!--termina primer row principal -->



<script>
//acceder
$('#estado_cli').val('<?php echo $cliente->estado_cli;?>');
$('#fk_id_pais').val('<?php echo $cliente->fk_id_pais;?>');
</script>

<script type="text/javascript">
    $("#frm_nuevo_cliente").validate({
      rules:{
        fk_id_pais:{
          required:true
        },
        identificacion_cli:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        apellido_cli:{
          letras:true,
          required:true

        },
        nombre_cli:{
          letras:true,
          required:true

        },
        telefono_cli:{
          required:true,
          minlength:13,
          maxlength:13,
          digits:true
        },
        direccion_cli:{
          required:true

        },
        email_cli:{
          required:true
        },
        estado_cli:{
          required:true
        }
      },
      messages:{
        fk_id_pais:{
          required:"Por favor seleccione el pais"
        },
        identificacion_cli:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },
        apellido_cli:{
          required:"Por favor ingrese el apellido",
          letras:"El nombre solo acepta letras"
        },
        nombre_cli:{
          required:"Por favor ingrese el nombre",
          letras:"El nombre solo acepta letras"
        },
        telefono_cli:{
          required:"Por favor ingrese el número de telefono",
          minlength:"El telefono debe tener mínimo 13 digitos",
          maxlength:"El telefono debe tener máximo 13 digitos",
          digits:"El telefono solo acepta números"
        },
        direccion_cli:{
          required:"Por favor ingrese la direccion"
        },
        email_cli:{
          required:"Por favor ingrese el correo electronico"
        },
        estado_cli:{
          required:"Por favor seleccione el estado"
        }
      }
    });
</script>
