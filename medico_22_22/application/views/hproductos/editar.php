<br>
<div class="row">
  <div class="col-md-12 text-center">
    <h2>Editar Examen Clinico</h2>
  </div>
</div>
<div class="row" style="margin: 0 20px 0 20px;">
  <div class="col-md-12">
    <div class="row">

      <!--Primera columna -->
      <br>
      <div class="col-md-6">

        <form action="<?php echo site_url(); ?>/hproductos/procesarActualizacion" method="post" id="frm_nuevo_hproductos">
          <input type="hidden" class="form-control" value="<?php echo $hproducto->id_hpro; ?>" name="id_hpro" id="id_hpro"  placeholder="Por favor ingrese la identificación">
          <br>
          <br>
          <label for="">NOMBRE</label>
          <input class="form-control"  type="text" name="nombre_hpro" value="<?php echo $hproducto->nombre_hpro; ?>" id="nombre_hpro" placeholder="Por favor Ingrese el apellido">
          <br>
          <br>
          <label for="">VALOR PREDETERMINADO</label>
          <input class="form-control"  type="number" name="cantidad_hpro" value="<?php echo $hproducto->cantidad_hpro; ?>" id="cantidad_hpro" placeholder="Por favor Ingrese el nombre">
          <br>
          <br>
          <label for="">PRECIO</label>
          <input class="form-control"  type="decimal" name="precio_hpro" value="<?php echo $hproducto->precio_hpro; ?>" id="precio_hpro" placeholder="Por favor Ingrese el nombre">
          <br>
          <br>
          <label for="">DESCRIPCION</label>
          <input class="form-control"  type="text" name="descripcion_hpro" value="<?php echo $hproducto->descripcion_hpro; ?>" id="descripcion_hpro" placeholder="Por favor Ingrese el telefono">
          <br>
          <br>


          <label for="">CATEGORIA</label>
          <select class="form-control" name="fk_id_hcat" id="fk_id_hcat" required>
            <option value="">--Seleccione la categoria--</option>
            <?php if ($listadoHcategorias): ?>
              <?php foreach ($listadoHcategorias->result() as $hcategoriaTemporal): ?>
                <option value="<?php echo $hcategoriaTemporal->id_hcat; ?>">
                  <?php echo $hcategoriaTemporal->nombre_hcat; ?>
                </option>

              <?php endforeach; ?>
            <?php endif; ?>
          </select>

          <br>

          <button class="btn btn-primary" name="button" type="submit"> <a style=" color:white;">Actualizar</a> </button>
          &nbsp&nbsp
          <button class="btn btn-primary"> <a href="<?php echo site_url(); ?>/hproductos/index" style=" color:white;"><i class="fa fa-times"> Cancelar </i></a> </button>
          <br><br>
        </form>

      </div><br>


  </div>  <!--termina primer row interno -->





</div>  <!--cierre col-md-12 -->
</div> <!--termina primer row principal -->



<script>

$('#fk_id_hcat').val('<?php echo $hproducto->fk_id_hcat;?>');
</script>

<script type="text/javascript">
    $("#frm_nuevo_hproductos").validate({
      rules:{
        nombre_hpro:{
          required:true
        },
        cantidad_hpro:{
          required:true,
          minlength:1,
          maxlength:10,
          digits:true

        },
        precio_hpro:{
          requerid:true,
          number:true
        },
        descripcion_hpro:{
          required:true
        },
        fk_id_hcat:{
          required:true
        }
      },
      messages:{
        nombre_hpro:{
          required:"Por favor ingrese el nombre"
        },
        cantidad_hpro:{
          requerid:"Por favor ingrese la cantidad",
          minlength:"La cantidad minina es 1",
          maxlength:"La cantidad maxima es 10",
          digits:"Solo se acepta números"
        },

        precio_hpro:{
          requerid:"Por favor ingrese el precio",
          number:"Ingrese numeros decimales"
        },

        descripcion_hpro:{
          required:"Por favor ingrese la descripcion"
        },
        fk_id_hcat:{
          required:"Por favor seleccione una categoría"
        }
      }
    });
</script>
