<br>
<center>
    <h2>Listado de Especialidades</h2>
</center>
<hr>
<br>
<center>



<div class="row" style=" margin: 0 20px 0 20px;">

  <div class="col-md-6 text-center" >
    <br>
     <button class="btn btn-primary btn-lg"><a href="<?php echo site_url(); ?>" style=" color:white;"><i class="fa fa-angle-left"> Volver </i></a> </button>
   </div>

  <div class="col-md-6 text-center" style="padding-top:30px;">

    <button class="btn btn-success"> <a href="<?php echo site_url(); ?>/empleados/nuevo " style=" color:white;"> <i class="fa fa-plus"> Agregar </i> </a> </button>
  </div>

</div>
<br>
<br>

<?php if ($listadoEspecialidades): ?>

  <table class="table table-dark table-hover">
    <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">NOMBRE</th>
         <th class="text-center">DESCRIPCION</th>
      <th class="text-center">OPCIONES</th>
    </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoEspecialidades->result() as $filaEspecialidad): ?>
        <tr>
          <td class="text-center"> <?php echo $filaProducto->id_pro; ?></td>
          <td class="text-center"> <?php echo $filaProducto->nombre_pro; ?></td>
          <td class="text-center"> <?php echo $filaProducto->descripcion_pro; ?></td>

          <td class="text-center">
            <a onclick="return confirm('¿Esta seguro de eliminar?');"href="<?php echo site_url();?>/especialidades/Eliminacion/<?php echo $filaProducto->id_pro; ?>"class="btn btn-danger"><i class="fa fa-trash"></i></a>
            <a   class="btn btn-success" onclick ="return confirm('¿Esta seguro de editar');"  href="<?php echo site_url() ?>/especialidades/editar/<?php echo $filaProducto->id_pro; ?>"><i class="fa fa-pen"></i></a>

             </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron productos registrados</h3>

  </div>
<?php endif; ?>
