<!-- JQUERY -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!--FRAMEWORK BOOTSTRAP para el estilo de la pagina-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<scriptsrc="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<!-- Los iconos tipo Solid de Fontawesome-->
<linkrel="stylesheet"href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
<scriptsrc="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

<!-- Nuestro css-->
<linkrel="stylesheet"type="text/css"href="static/css/index.css">
<br>
<br>
<section class="vh-100" style="background-color: #d1ecf1;">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col col-xl-10">
        <div class="card" style="border-radius: 1rem;">
          <div class="row g-0">
            <div class="col-md-6 col-lg-5 d-none d-md-block">
              <img src="https://i.pinimg.com/originals/7a/44/27/7a442759ba01432ae8e0b7e47c36d637.png"
                alt="login form" class="img-fluid" style="border-radius: 1rem 0 0 1rem;" />
            </div>
            <div class="col-md-6 col-lg-7 d-flex align-items-center">
              <div class="card-body p-4 p-lg-5 text-black">


                <center>
                      <h2 class="text-info">UTC-LATACUNGA</h2>
                </center>
<br>
            <center>  <h5 class="fw-normal mb-3 pb-3" style="letter-spacing: 1px;">Iniciar secion en su cuenta</h5> </center>


                <form class=""
                action="<?php echo site_url(); ?>/seguridades/validarAcceso" method="post">
                <div style="border:3px soli #CDD1C3; with:40%; margin-top:4px; margin-left: 7%; margin-right: 7%">
                <div class="col-md-12">

                  <center><label for=""><b>Email:</b></label> <br>
                  </div></center>


                  <input  style="width:400px" class="form-control" type="email" name="email_usu"
                  id="email_usu" value="" placeholder="Ingrese su email"
                  required><br>
                </div>

  <div class="col-md-12">
                  <center><label for=""><b>Contraseña:</b></label> <br>
                  </center>
                  <center><input style="width:400px" class="form-control" type="password" name="password_usu"
                  id="password_usu" value="" placeholder="Ingrese su contraseña"
                  required><br></center>

  </div>
                  <button  type="submit" name="button" class="btn btn-dark btn-lg btn-block"> Ingresar </button>
                </form>
                <?php if ($this->session->flashdata("error")): ?>
                <script type="text/javascript">
                    alert("<?php echo $this->session->flashdata("error"); ?> ");
                </script>
                <?php endif; ?>

                <script type="text/javascript">
                iziToast.warning({
  title: 'Caution',
  message: 'You forgot important data',
});
                </script>
</span>


                  <a class="small text-muted" href="#!">Forgot password?</a>
                  <p class="mb-5 pb-lg-2" style="color: #393f81;">Don't have an account? <a href="#!"
                      style="color: #393f81;">Register here</a></p>
                  <a href="#!" class="small text-muted">Terms of use.</a>
                  <a href="#!" class="small text-muted">Privacy policy</a>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
