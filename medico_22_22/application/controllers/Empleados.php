<?php
class Empleados extends CI_Controller{

  public function __construct(){
    //llamamos al constructor(el constructor no devuelve valores)
    parent::__construct();
    //cargamos el modelo en el controlador
    $this->load->model('empleado');
    $this->load->model('especialidad');
  }//cierre del constructor

  public function index(){
    if ($this->session->userdata("c0nectadoUTC")) {

		}else{
			redirect("seguridades/formularioLogin");
		}
    $data["listadoEmpleados"]=$this->empleado->consultarTodos();
    $this->load->view('header');
    $this->load->view('empleados/index',$data);
    $this->load->view('footer');
  }//cierre de la funcion index
  public function nuevo(){
    if ($this->session->userdata("c0nectadoUTC")) {

		}else{
			redirect("seguridades/formularioLogin");
		}
    $data["listadoProductos"]=$this->especialidad->consultarTodos();
    $this->load->view('header');
    $this->load->view('empleados/nuevo',$data);
    $this->load->view('footer');

  }
  public function editar($id_emp){
    if ($this->session->userdata("c0nectadoUTC")) {

		}else{
			redirect("seguridades/formularioLogin");
		}
    $data["listadoProductos"]=$this->especialidad->consultarTodos();
    $data["empleado"]=$this->empleado->consultarPorId($id_emp);
    $this->load->view("header");
    $this->load->view("empleados/editar",$data);
    $this->load->view("footer");
  }
  public function guardarEmpleado(){
    $datosNuevoEmpleado=array(
      "nombre_emp"=>$this->input->post("nombre_emp"),
      "apellido_emp"=>$this->input->post("apellido_emp"),
      "direccion_emp"=>$this->input->post("direccion_emp"),
      "email_emp"=>$this->input->post("email_emp"),
      "telefono_emp"=>$this->input->post("telefono_emp"),
      "fk_id_pro"=>$this->input->post("fk_id_pro")

    );
    if($this->empleado->insertar($datosNuevoEmpleado)){
      $this->session->set_flashdata("confirmacion","Doctor(a) ingresado correctamente");
    }else{
      $this->session->set_flashdata("error","Error al ingresar");
    }
    redirect("empleados/index");
    }//cierre funcion guardarEmpleado

public function procesarEliminacion($id_emp){

  if($this->empleado->eliminar($id_emp)){

    $this->session->set_flashdata('eliminacion',"Doctor(a) eliminado exitosamente.");

  }else{
    $this->session->set_flashdata("error","Error al procesar intente nuevamente.");
  }
redirect("empleados/index");
}//cierre funcion procesarElimacion

public function procesarActualizacion(){
$id_emp=$this->input->post("id_emp");
$datosEmpleadoEditado=array(
  "nombre_emp"=>$this->input->post("nombre_emp"),
  "apellido_emp"=>$this->input->post("apellido_emp"),
  "direccion_emp"=>$this->input->post("direccion_emp"),
  "email_emp"=>$this->input->post("email_emp"),
  "telefono_emp"=>$this->input->post("telefono_emp"),
  "fk_id_pro"=>$this->input->post("fk_id_pro")
);

if($this->empleado->actualizar($id_emp,$datosEmpleadoEditado)){
  $this->session->set_flashdata('edicion',"Doctor(a) editado exitosamente.");

}else{
$this->session->set_flashdata("error","Error al procesar intente nuevamente.");
}
redirect("empleados/index");
}

}//cierre de la clase



 ?>
