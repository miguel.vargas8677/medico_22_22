<?php
  class Seguridades extends CI_Controller {

    function __construct() {
      parent::__construct();
      $this->load->model("usuario");
    }
//funcion que encarga de erenderizar
//la vista con el formulario de Login
    public function formularioLogin(){
      $this->load->view("seguridades/formularioLogin");
    }
    //funcion que valida las credenciales ingresadas
    public function validarAcceso(){
      $email_usu=$this->input->post("email_usu");
      $password_usu=$this->input->post("password_usu");
      $usuario=$this->usuario->buscarUsuarioPorEmailPassword($email_usu,$password_usu);
      if($usuario){
        //cuando el email y contraseña son correctas
        if($usuario->estado_usu>0){//validando estado
          //si esta activo
          ///creando la variable desision con el nombre c0nectdoUTC
          $this->session->set_userdata("c0nectadoUTC",$usuario);
          $this->session->set_flashdata("bienvenida","Saludos, bienvenido al sistema");
          redirect("clientes/index");//la primera vista que vera el usuario
        }else {
          $this->session->set_flashdata("error","Usuario Bloqueado");
          redirect("seguridades/formularioLogin");
        }
      }else{//cuando no existe
        $this->session->set_flashdata("error","Email o contraseña incorrectas");
        redirect("seguridades/formularioLogin");
      }

    }
    public function cerrarSesion(){
      $this->session->sess_destroy();//Matando la sesiones
      redirect("seguridades/formularioLogin");
    }
  }//cierre de la clase


 ?>
