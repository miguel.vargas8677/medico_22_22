<?php
class Especialidades extends CI_Controller{
  public function __construct(){

    parent::__construct();
    $this->load->model('especialidad');
  }//cierre del constructor

public function index(){
  if ($this->session->userdata("c0nectadoUTC")) {

  }else{
    redirect("seguridades/formularioLogin");
  }
$data["listadoEspecialidades"]=$this->especialidad->consultarTodos();
$this->load->view('header');
$this->load->view('especialidades/index',$data);
$this->load->view('footer');
}//cierre de la funcion Index
public function nuevo(){
  if ($this->session->userdata("c0nectadoUTC")) {

  }else{
    redirect("seguridades/formularioLogin");
  }
  $this->load->view('header');
  $this->load->view('especialidades/nuevo');
  $this->load->view('footer');

}//cierre de la funcion nuevo

public function guardarEspecialidad(){
$datosNuevoEspecialidad=array(
  "nombre_pro"=>$this->input->post("nombre_pro"),
  "descripcion_pro"=>$this->input->post("descripcion_pro")
  );

 if($this->especialidad->insertar($datosNuevoEspecialidad)){

   redirect('especialidades/index');

 }else{
    echo"Error al insertar datos";

 }

}//cierre de la funcion guardarProducto

public function Eliminacion($id_pro){
  if($this->especialidad->eliminar($id_pro)){

    redirect("especialidades/index");

  }else{

    echo"Error al eliminar";

  }

}//cierre de la funcion Eliminacion
public function consulta(){
  $listadoEspecialidades=$this->db->get('especialidad');
  if ($listadoEspecialidades->num_rows()>0) {
    return $listadoEspecialidades;
  }else {
    return false;
  }
}
public function editar($id_pro){
  if ($this->session->userdata("c0nectadoUTC")) {

  }else{
    redirect("seguridades/formularioLogin");
  }

      $data["especialidad"]=$this->especialidad->consultarPorId($id_pro);
      $this->load->view("header");
      $this->load->view("especialidades/editar",$data);
      $this->load->view("footer");
}
public function procesarActualizacion(){
    $id_pro=$this->input->post("id_pro");
    $datosNuevoEspecialidad=array(
      "nombre_pro"=>$this->input->post("nombre_pro"),
      "descripcion_pro"=>$this->input->post("descripcion_pro")
      );

    if ($this->especialidad->actualizar($id_pro,$datosNuevoEspecialidad)) {
      redirect("especialidades/index");
      // code...
    }else {
      echo "Error de actuliazacions";
    }
  }
}//cierre de la clase Productos





 ?>
