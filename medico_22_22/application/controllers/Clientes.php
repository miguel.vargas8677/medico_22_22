<?php
      class Clientes extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("cliente");
            $this->load->model("pais");
        }

        public function index(){
          if ($this->session->userdata("c0nectadoUTC")) {

      		}else{
      			redirect("seguridades/formularioLogin");
      		}
          $data["listadoClientes"]=$this->cliente->consultarTodos();
          $this->load->view("header");
          $this->load->view("clientes/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          if ($this->session->userdata("c0nectadoUTC")) {

      		}else{
      			redirect("seguridades/formularioLogin");
      		}
          $data["listadoPaises"]=$this->pais->consultarTodos();
          $this->load->view("header");
          $this->load->view("clientes/nuevo",$data);
          $this->load->view("footer");
        }
        public function editar($id_cli){
          if ($this->session->userdata("c0nectadoUTC")) {

      		}else{
      			redirect("seguridades/formularioLogin");
      		}
          $data["listadoPaises"]=$this->pais->consultarTodos();
          $data["cliente"]=$this->cliente->consultarPorId($id_cli);
          $this->load->view("header");
          $this->load->view("clientes/editar",$data);
          $this->load->view("footer");
        }

        public function guardarCliente(){
            $datosNuevoCliente=array(
                "identificacion_cli"=>$this->input->post("identificacion_cli"),
                "apellido_cli"=>$this->input->post("apellido_cli"),
                "nombre_cli"=>$this->input->post("nombre_cli"),
                "telefono_cli"=>$this->input->post("telefono_cli"),
                "direccion_cli"=>$this->input->post("direccion_cli"),
                "email_cli"=>$this->input->post("email_cli"),
                "estado_cli"=>$this->input->post("estado_cli"),
                "fk_id_pais"=>$this->input->post("fk_id_pais")
            );
            if($this->cliente->insertar($datosNuevoCliente)){
                //echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","Cliente insertado exitosamente.");
            }else{

                $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");

            }

            redirect("clientes/index");
        }

        public function procesarEliminacion($id_cli){
          if ($this->cliente->eliminar($id_cli)) {
            $this->session->set_flashdata('eliminacion',"Cliente eliminado exitosamente.");
          }else{
            $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
          }
          redirect("clientes/index");
        }

        public function procesarActualizacion(){
          $id_cli=$this->input->post("id_cli");
          $datosClienteEditado=array(
              "identificacion_cli"=>$this->input->post("identificacion_cli"),
              "apellido_cli"=>$this->input->post("apellido_cli"),
              "nombre_cli"=>$this->input->post("nombre_cli"),
              "telefono_cli"=>$this->input->post("telefono_cli"),
              "direccion_cli"=>$this->input->post("direccion_cli"),
              "email_cli"=>$this->input->post("email_cli"),
              "estado_cli"=>$this->input->post("estado_cli"),
              "fk_id_pais"=>$this->input->post("fk_id_pais")
          );

          if($this->cliente->actualizar($id_cli,$datosClienteEditado)){
            $this->session->set_flashdata('edicion',"Cliente editado exitosamente.");
          }else{
            $this->session->set_flashdata("error","Error al procesar intente nuevamente.");
          }
          redirect("clientes/index");
         }

        }//cierre de la clase
?>
