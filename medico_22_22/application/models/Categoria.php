<?php

class Categoria extends CI_Model{
  public function __construct(){
    parent::__construct();
  }

  public function insertar($datos){
    return $this->db->insert('hcategoria',$datos);
  }

  public function actualizar($id_hcat,$datos){
  $this->db->where("id_hcat",$id_hcat);
  return $this->db->update("hcategoria",$datos);
  }

  public function consultarTodos(){
    $listadoCategorias=$this->db->get("hcategoria");
    if($listadoCategorias->num_rows()>0){
      return $listadoCategorias;
    }else {
      return false;
    }
  }

  public function eliminar($id_hcat){
    $this->db->where('id_hcat',$id_hcat);
    return $this->db->delete("hcategoria");
  }
  
  public function consultarPorId($id_hcat){
    $this->db->where("id_hcat",$id_hcat);
    $categoria=$this->db->get('hcategoria');
    if ($categoria->num_rows()>0) {
        return $categoria->row();
    }else{
      return false;
    }

  }







}


 ?>
